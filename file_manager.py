# Your code goes here / Letsgooo pour le 20/20 *nathzer putting his sunglasse
# Give a gift for Tom, exercise 5 was really hard and he deserves it

import json


def count(filename):
    """Ouvre le fichier JSON, et compte le nombre d'éléments présents dans la structure."""
    with open('film.JSON', 'r', encoding='utf-8') as f:
        json_data = json.load(f)
    return len(json_data)


def written_by(filename, name):
    """Ouvre le fichier JSON, et cherche les enregistrements écrits par Vince Gilligan"""
    with open('film.JSON', 'r', encoding='utf-8') as f:
        json_data = json.load(f)
        films = []

        for lines in range(len(json_data)):
            if name in json_data[lines]['Writer']:
                films.append(json_data[lines])
    return films


def longest_title(filename):
    """Ouvre le fichier JSON, et cherche le film avec le nom le plus long"""
    with open(filename) as f:
        json_data = json.load(f)
    title = " "
    index_reference = 0
    for i in range(len(json_data)):
        if 'Title' not in json_data[i]:
            i += 1
        elif json_data[i]['Title'] == "N/A":
            i += 1
        elif len(json_data[i]['Title']) > len(title):
            title = json_data[i]['Title']
            index_reference = i
    return json_data[index_reference]


def best_rating(filename):
    """Ouvre le fichier JSON, et cherche l'enregistrement avec le meilleur imdbRating """
    with open(filename) as f:
        json_data = json.load(f)
    rating = 0.0
    index_reference = 0
    for i in range(len(json_data)):
        if 'imdbRating' not in json_data[i]:
            i += 1
        elif json_data[i]['imdbRating'] == "N/A":
            i += 1
        elif float(json_data[i]['imdbRating']) > rating:
            rating = float(json_data[i]['imdbRating'])
            index_reference = i
    return json_data[index_reference]

# This one couldn't be done without the help of Tom ;) (next one tho but not that much compared to the one down there)
def latest_film(filename):
    """Ouvre le fichier JSON, et cherche l'enregistrement avec le Year le plus récent, retourne le nom du film
    concerné """
    with open(filename) as f:
        json_data = json.load(f)
    beginning = 0
    index_reference = 0
    for i in range(len(json_data)):
        if 'Year' not in json_data[i]:
            i += 1
        elif json_data[i]['Year'] == "N/A":
            i += 1
        elif json_data[i]['Year'][(len(json_data[i]['Year']) - 1)] != type(beginning):
            if int(json_data[i]['Year'][:4]) >= beginning: 
                beginning = int(json_data[i]['Year'][:4])
                index_reference = i
        elif int(json_data[i]['Year'][len(json_data[i]['Year']) - 4:]) >= beginning:
            beginning = int(json_data[i]['Year'][len(json_data[i]['Year']) - 4:])
            index_reference = i
    return json_data[index_reference]['Title']

    
def find_per_genre(filename, kind):
    """Ouvre le fichier JSON, et cherche le nombre d'enregistrement par genre"""
    with open(filename) as f:
        json_data = json.load(f)
    list_film_by_kind = []
    for i in range(len(json_data)):
        if 'Genre' not in json_data[i]:
            i += 1
        elif json_data[i]['Genre'] == "N/A":
            i += 1
        elif kind in json_data[i]['Genre']:
            list_film_by_kind.append(json_data[i])
    return list_film_by_kind